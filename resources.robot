*** Settings ***
Documentation    Resources dos testes da Secretaria On-line
Library    SeleniumLibrary

*** Variables ***
# Ambiente de testes da Secretaria On-line
${GRR_ALUNO}         GRR11111111
${SENHA_ALUNO}       123
${TEXTO_PAG_LOGIN}   //h3[contains(.,'Secretaria On-line do SEPT - Login')]
${TEXTO_PAG_INICIAL} 
${LINK_CONTATO}      //a[contains(.,'Contato')]

*** Keywords ***
Abrir browser
    Open Browser    browser=chrome
    Maximize Browser Window

Fechar browser
    Capture Page Screenshot
    Close Browser

Acessar Secretaria Online no endereço "${URL}"
    Go To    ${URL}
    Wait Until Element Is Visible    ${TEXTO_PAG_LOGIN}

Acessar página de contato
    Click Element    locator=${LINK_CONTATO}

Verificar se contato é exibido
    Element Should Be Visible    locator=//h3[contains(.,'Contato com os Administradores do Sistema')]


