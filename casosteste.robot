*** Settings ***

Documentation    Demonstração do RobotFramework com a Secretaria On-line
Test Setup       Abrir browser
Test Teardown    Fechar browser
Resource         resources.robot

*** Test Cases ***

CT01: Verificar contatos da secretaria online
    [Documentation]    Verifica se os contatos são corretamente mostrados
    [Tags]    contatos
    Acessar Secretaria Online no endereço "http://200.236.3.198:28080/secretariaonline2/Home"
    Acessar página de contato
    Verificar se contato é exibido